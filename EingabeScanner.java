import java.util.*; // oder java.util.Scanner;

/**
 * Beispiel einer Tastatureingabe von der Konsole mithilfe der Klasse
 * java.util.Scanner.
 * @author FHDWBAP
 *
 */
public class EingabeScanner 
{

	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Bitte einen int-Wert und eine Zeichenkette eingeben: ");
		System.out.println("Ende durch Eingabe von \"ende\".");

		int wert = scanner.nextInt();
		System.out.println("Die int-Zahl: " + wert);
		
		String s;   
		
		do
		{
			s = scanner.next();
			
			System.out.println("Der String:   " + s + "(" + s.length() + ")");
			
			if (s.equals("ende"))
			{
			   System.out.println("Ende erreicht.");
			}
			
		} while (!s.equals("ende"));
		
	    System.out.println("Demo beendet.");
	}

}
